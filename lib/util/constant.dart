class Constants {
  static const String appName = "AppTechbusiness";

  static const String urlAuthority =
      'ec2-18-221-50-26.us-east-2.compute.amazonaws.com:8089';

  //path
  static const String pathBase = '/api/tech_business';
  static const String contentTypeHeader = 'application/json; charset=utf-8';
  static const String authorizationHeader = 'Bearer ';

  //Url Backend admissiondatasheet
  static const String urlAdmission = pathBase + '/admission';

  //route
  static const String homeRoute = "/";
  static const String homeRouteUi = "home_ui";
  static const String detailAdmissionDataSheetUiRoute =
      "detail_admission_data_sheet_ui";

  static const String admissionDataSheetSuccess =
      'La ficha de ingreso fue encontrada';
  static const String admissionDataSheeFail =
      'El id es incorrecto, intentelo de nuevo';

  //appbar
  static const String admissionDataAppbar = 'Buscar La ficha de ingreso por id';

  //button
  static const String btnCancel = 'Cancelar';

  //message
  static const String succesMessage = 'Busqueda exitosa';

  //appBar
  static const String appBarDetailAdmissionDataSheet =
      'Detalles de la ficha de ingreso';

  //card text
  static const String cardTextEquipment = 'Equipo';
  static const String cardTextMark = 'Marca: ';
  static const String cardTextModel = 'Modelo: ';
  static const String cardTextType = 'Tipo: ';
  static const String cardTextHardDisk = 'Disco duro: ';
  static const String cardTextRam = 'memoria ram: ';
  static const String cardTextBoar = 'Boar: ';
  static const String cardTextReaderCd = 'readerCd: ';
  static const String cardTextOther = 'Otros: ';
  static const String cardTextProcessor = 'Procesador: ';
  static const String cardTextService = 'Servicio';
}
