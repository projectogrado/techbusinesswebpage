import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusiness/util/constant.dart';
import 'model/admission_data_sheet_model.dart';

class DetailAdmissionDataSheetUi extends StatefulWidget {
  final AdmissionDataSheet admissionDataSheet;

  const DetailAdmissionDataSheetUi({Key key, this.admissionDataSheet})
      : super(key: key);

  @override
  DetailAdmissionDataSheetUiState createState() =>
      DetailAdmissionDataSheetUiState(admissionDataSheet: admissionDataSheet);
}

class DetailAdmissionDataSheetUiState extends State<DetailAdmissionDataSheetUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  DetailAdmissionDataSheetUiState({this.admissionDataSheet});
  AdmissionDataSheet admissionDataSheet;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(Constants.appBarDetailAdmissionDataSheet),
        backgroundColor: Color(0xff50459B),
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacementNamed(context, Constants.homeRouteUi);
          },
          child: Icon(
            Icons.arrow_back,
            size: 24,
          ),
        ),
      ),
      body: Stack(fit: StackFit.expand, children: <Widget>[
        Align(
            child: SizedBox(
          width: 400.0,
          height: 600.0,
          child: SingleChildScrollView(
            child: Container(
              child: Center(
                child: Column(
                  children: <Widget>[
                    Card(
                      elevation: 50,
                      shadowColor: Colors.black,
                      child: Container(
                        padding: EdgeInsets.all(32.0),
                        child: Column(
                          children: <Widget>[
                            Text(
                              Constants.cardTextEquipment,
                              style: TextStyle(
                                fontSize: 30,
                                color: Colors.green[900],
                                fontWeight: FontWeight.w500,
                              ), //Textstyle
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              Constants.cardTextModel +
                                  admissionDataSheet.equipment.model,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.green[900],
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              Constants.cardTextMark +
                                  admissionDataSheet.equipment.mark,
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.green[900],
                                fontWeight: FontWeight.w500,
                              ),
                              textAlign: TextAlign.left,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              Constants.cardTextType +
                                  admissionDataSheet.equipment.type,
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.green[900],
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              Constants.cardTextHardDisk +
                                  admissionDataSheet.equipment.hardDisk,
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.green[900],
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              Constants.cardTextRam +
                                  admissionDataSheet.equipment.ram,
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.green[900],
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              Constants.cardTextBoar +
                                  admissionDataSheet.equipment.board,
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.green[900],
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              Constants.cardTextProcessor +
                                  admissionDataSheet.equipment.processor,
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.green[900],
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: admissionDataSheet.workServices.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                            elevation: 50,
                            shadowColor: Colors.black,
                            child: Container(
                                padding: EdgeInsets.all(32.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      Constants.cardTextService,
                                      style: TextStyle(
                                        fontSize: 30,
                                        color: Colors.green[900],
                                        fontWeight: FontWeight.w500,
                                      ),
                                      //Textstyle
                                    ),
                                    Text(
                                      admissionDataSheet
                                          .workServices[index].services.name,
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.green[900],
                                        fontWeight: FontWeight.w500,
                                      ), //Textstyle
                                    ),
                                    Text(
                                      admissionDataSheet.workServices[index]
                                          .services.description,
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.green[900],
                                        fontWeight: FontWeight.w500,
                                      ), //Textstyle
                                    ),
                                    Text(
                                      admissionDataSheet
                                          .workServices[index].services.status
                                          .toString(),
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.green[900],
                                        fontWeight: FontWeight.w500,
                                      ), //Textstyle
                                    ),
                                  ],
                                )));
                      },
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: admissionDataSheet
                          .workServices[0].services.checklist.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                            elevation: 50,
                            shadowColor: Colors.black,
                            child: Container(
                                padding: EdgeInsets.all(32.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      'check list',
                                      style: TextStyle(
                                        fontSize: 30,
                                        color: Colors.green[900],
                                        fontWeight: FontWeight.w500,
                                      ),
                                      //Textstyle
                                    ),
                                    Text(
                                      admissionDataSheet.workServices[0]
                                          .services.checklist[index].name,
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.green[900],
                                        fontWeight: FontWeight.w500,
                                      ), //Textstyle
                                    ),
                                    Text(
                                      admissionDataSheet
                                          .workServices[0]
                                          .services
                                          .checklist[index]
                                          .description,
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.green[900],
                                        fontWeight: FontWeight.w500,
                                      ), //Textstyle
                                    ),
                                    Text(
                                      admissionDataSheet.workServices[0]
                                          .services.checklist[index].status
                                          .toString(),
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.green[900],
                                        fontWeight: FontWeight.w500,
                                      ), //Textstyle
                                    ),
                                  ],
                                )));
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ))
      ]),
    );
  }
}
