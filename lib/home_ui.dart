import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusiness/bloc/admissiondatasheet_bloc.dart';
import 'package:techbusiness/detail_admission_data_sheet_ui.dart';
import 'package:techbusiness/model/admission_data_sheet_model.dart';
import 'package:techbusiness/util/constant.dart';

class HomeUi extends StatefulWidget {
  const HomeUi({Key key}) : super(key: key);

  @override
  HomeUiState createState() => HomeUiState();
}

class HomeUiState extends State<HomeUi> with SingleTickerProviderStateMixin {
  final GlobalKey<FormState> _formRegister = GlobalKey<FormState>();
  AdmissionDataSheet admissionDataSheet = AdmissionDataSheet();
  AdmissionDataSheetBloc admissionDataSheetBloc;

  @override
  void initState() {
    super.initState();
    admissionDataSheetBloc = AdmissionDataSheetBloc();
  }

  _handleSubmitted() {
    var data = _formRegister.currentState;
    data.save();
    admissionDataSheetBloc
        .getAdmissionDataSheetById(admissionDataSheet.id)
        .then((apiResponse) {
      if (apiResponse.statusResponse == 200) {
        showDialogSuccess(context, 'Guardado exitoso');
        admissionDataSheet = apiResponse.payload;

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailAdmissionDataSheetUi(
                admissionDataSheet: admissionDataSheet),
          ),
        );
      } else {
        var err = apiResponse.message;
        showAlert(context, err);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(Constants.admissionDataAppbar),
          backgroundColor: Colors.indigo,
        ),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: _formRegister,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 120.0),
                        ),
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              icon: Icon(Icons.person_search),
                              hintText: 'Id'),
                          keyboardType: TextInputType.number,
                          onSaved: (String value) {
                            admissionDataSheet.id = int.parse(value);
                          },
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: ElevatedButton(
                            onPressed: () {
                              _handleSubmitted();
                            },
                            child: Text('Buscar'),
                          ),
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }

  void showDialogSuccess(BuildContext context, String message) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Center(child: Text('Success')),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.check,
                  color: Color(0xf74ba826),
                ),
                Expanded(
                  child: Text(
                    message,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                )
              ],
            ),
            actions: <Widget>[
              TextButton(
                child: Text(Constants.btnCancel),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  void showAlert(BuildContext context, String message) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Center(child: Text('error')),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.error,
                  color: Color(0xf74e0009),
                ),
                Expanded(
                  child: Text(
                    message,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                )
              ],
            ),
            actions: <Widget>[
              TextButton(
                child: Text(Constants.btnCancel),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }
}
