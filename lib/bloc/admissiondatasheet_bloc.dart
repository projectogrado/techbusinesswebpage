import 'dart:async';

import 'package:techbusiness/model/api_response_model.dart';
import 'package:techbusiness/repository/repository.dart';
import 'package:techbusiness/util/constant.dart';

class AdmissionDataSheetBloc {
  final _repository = Repository();
  ApiResponse _apiResponse = ApiResponse();

  Future<ApiResponse> getAdmissionDataSheetById(int id) async {
    _apiResponse = await _repository.getAdmissionDataSheetById(id);
    if (_apiResponse.statusResponse == 200) {
      _apiResponse.message = Constants.admissionDataSheetSuccess;
    } else {
      _apiResponse.message = Constants.admissionDataSheeFail;
    }
    return _apiResponse;
  }
}
