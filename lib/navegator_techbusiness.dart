import 'package:flutter/cupertino.dart';
import 'package:techbusiness/util/constant.dart';

class TechBusinessNavigator {
  static void goToHome(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Constants.homeRoute, (Route<dynamic> route) => false);
  }

  static void goToHomeUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.homeRouteUi);
  }

  static void goToDetailAdmissionDataSheetUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.detailAdmissionDataSheetUiRoute);
  }
}
