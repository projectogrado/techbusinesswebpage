import 'package:techbusiness/model/process_model.dart';

class Service {
  int id;
  String name;
  String description;
  bool status;
  List<Process> checklist;

  Service({this.id, this.name, this.description, this.status, this.checklist});

  factory Service.fromJson(Map<String, dynamic> parsedJson) {
    return Service(
        id: parsedJson['id'],
        name: parsedJson['name'],
        description: parsedJson['description'],
        status: parsedJson['status'],
        checklist: List<Process>.from(
            parsedJson['checklist'].map((x) => Process.fromJson(x))));
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'status': status,
        'checklist': checklist
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'description': description,
        'status': status,
        'checklist': checklist
      };
}
