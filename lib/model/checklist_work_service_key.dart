class ChecklistWorkServiceKey {
  int checklistId;
  int workServiceId;

  ChecklistWorkServiceKey({this.checklistId, this.workServiceId});

  factory ChecklistWorkServiceKey.fromJson(Map<String, dynamic> parsedJson) {
    return ChecklistWorkServiceKey(
        checklistId: parsedJson['checklistId'],
        workServiceId: parsedJson['workServiceId']);
  }

  Map<String, dynamic> toJson() =>
      {'checklistId': checklistId, 'workServiceId': workServiceId};
}
