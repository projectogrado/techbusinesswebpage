import 'package:techbusiness/model/process_model.dart';

class Checklist {
  int id;
  Process process;

  Checklist({this.id, this.process});

  factory Checklist.fromJson(Map<String, dynamic> parsedJson) {
    var processJson = parsedJson['process'];
    var getProcess = Process.fromJson(processJson);

    return Checklist(id: parsedJson['id'], process: getProcess);
  }

  Map<String, dynamic> toJson() => {'id': id, 'process': process.toJson()};
}
