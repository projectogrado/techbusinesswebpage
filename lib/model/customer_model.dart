class Customer {
  int id;
  String name;
  String lastName;
  String email;
  String address;
  String phone;
  String username;
  String password;
  bool status;
  String typeDocument;
  String document;

  Customer(
      {this.id,
      this.name,
      this.lastName,
      this.email,
      this.address,
      this.phone,
      this.username,
      this.password,
      this.status,
      this.document,
      this.typeDocument});

  factory Customer.fromJson(Map<String, dynamic> parsedJson) {
    return Customer(
      id: parsedJson['id'],
      name: parsedJson['name'],
      lastName: parsedJson['lastname'],
      email: parsedJson['email'],
      address: parsedJson['address'],
      phone: parsedJson['phone'],
      status: parsedJson['status'],
      document: parsedJson['document'],
      typeDocument: parsedJson['typeDocument'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'lastname': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'status': status,
        'document': document,
        'typeDocument': typeDocument,
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'lastname': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'status': status,
        'document': document,
        'typeDocument': typeDocument,
      };
}
