import 'package:techbusiness/model/service_model.dart';
import 'checklist_work_service_model.dart';

class WorkService {
  int id;
  Service services;
  List<ChecklistWorkService> checklistWorkServices;

  WorkService({this.checklistWorkServices, this.id, this.services});

  factory WorkService.fromJson(Map<String, dynamic> parsedJson) {
    var checkListWork = parsedJson['checklistWorkServices'] as List;

    var checkListWorkService = checkListWork != null
        ? checkListWork.map((i) => ChecklistWorkService.fromJson(i)).toList()
        : List.empty();

    return WorkService(
      id: parsedJson['id'],
      services: Service.fromJson(parsedJson['services']),
      checklistWorkServices: checkListWorkService,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'service': services.toJson(),
        'checklistWorkServices': checklistWorkServices != null
            ? checklistWorkServices.map((i) => i.toJson()).toList()
            : List.empty(),
      };
}
