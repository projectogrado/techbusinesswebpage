import 'checklist_model.dart';
import 'checklist_work_service_key.dart';

class ChecklistWorkService {
  ChecklistWorkServiceKey id;
  bool status;
  String evidence;
  Checklist checklist;

  ChecklistWorkService({this.id, this.status, this.evidence, this.checklist});

  factory ChecklistWorkService.fromJson(Map<String, dynamic> parsedJson) {
    var checklistJson = parsedJson['checklist'];
    var getChecklist = Checklist.fromJson(checklistJson);

    var idJson = parsedJson['id'];
    var getId = ChecklistWorkServiceKey.fromJson(idJson);

    return ChecklistWorkService(
        id: getId,
        status: parsedJson['status'],
        evidence: parsedJson['evidence'],
        checklist: getChecklist);
  }

  Map<String, dynamic> toJson() => {
        'id': id.toJson(),
        'status': status,
        'evidence': evidence,
        'checklist': checklist.toJson()
      };
}
