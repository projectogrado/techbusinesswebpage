import 'package:techbusiness/model/api_response_model.dart';
import 'package:techbusiness/service/admission_data_sheet_api_service.dart';

class Repository {
  final admissionDataSheetApiService = AdmissionDataSheetApiService();

  //AdmissionDataSheet
  Future<ApiResponse> getAdmissionDataSheetById(int id) =>
      admissionDataSheetApiService.getAdmissionDataSheetById(id);
}
