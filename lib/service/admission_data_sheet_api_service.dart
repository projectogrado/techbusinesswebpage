import 'dart:convert';
import 'dart:io';

import 'package:techbusiness/model/admission_data_sheet_model.dart';
import 'package:techbusiness/model/api_response_model.dart';
import 'package:techbusiness/model/error_api_response_model.dart';
import 'package:http/http.dart' as http;
import 'package:techbusiness/util/constant.dart';

class AdmissionDataSheetApiService {
  ApiResponse _apiResponse;
  AdmissionDataSheet _admissionDataSheet;
  ErrorApiResponse _error;

  AdmissionDataSheetApiService();

  Future<ApiResponse> getAdmissionDataSheetById(int id) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var pathVariable = '/' + id.toString();
    var uri =
        Uri.http(Constants.urlAuthority, Constants.urlAdmission + pathVariable);

    var res = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
    });

    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;

    if (_apiResponse.statusResponse == 200) {
      _admissionDataSheet = AdmissionDataSheet.fromJson(resBody);
      _apiResponse.payload = _admissionDataSheet;
      return _apiResponse;
    }

    _error = ErrorApiResponse.fromJson(resBody);
    _apiResponse.payload = _error;
    return _apiResponse;
  }
}
