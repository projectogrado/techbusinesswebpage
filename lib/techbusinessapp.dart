import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusiness/util/constant.dart';

import 'detail_admission_data_sheet_ui.dart';
import 'home_ui.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: Constants.appName,
      initialRoute: Constants.homeRoute,
      routes: {
        Constants.homeRoute: (context) => HomeUi(),
        Constants.detailAdmissionDataSheetUiRoute: (context) =>
            DetailAdmissionDataSheetUi(),
        Constants.homeRouteUi: (context) => HomeUi(),
      },
    );
  }
}
